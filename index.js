const btn = document.querySelector('.btn');

btn.addEventListener('click', function(event){
  getIp();

});


async function getIp() {
  const response = await fetch('https://api.ipify.org/?format=json', {method: 'GET'});
  const { ip } = await response.json();
  const physAddress = await fetch (`http://ip-api.com/json/${ip}?lang=ru&fields=country,regionName,city,district,timezone`, {method: 'POST'});
  const {timezone, country, regionName, city, district} = await physAddress.json();
  console.log(timezone, country, regionName, city, district);

  document.body.innerHTML+= `<p>Континет: ${timezone}</p>
                             <p>Страна: ${country}</p>
                             <p>Регион: ${regionName}</p>
                             <p>Город: ${city}</p>
                             <p>Район города: ${district}</p> `
}